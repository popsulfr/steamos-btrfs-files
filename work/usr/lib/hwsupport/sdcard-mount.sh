#!/bin/bash

# Originally from https://serverfault.com/a/767079

# This script is called from our systemd unit file to mount or unmount
# a USB drive.

usage()
{
    echo "Usage: $0 {add|remove} device_name (e.g. sdb1)"
    exit 1
}

if [[ $# -ne 2 ]]; then
    usage
fi

ACTION=$1
DEVBASE=$2
DEVICE="/dev/${DEVBASE}"

MOUNT_LOCK="/var/run/sdcard-mount.lock"
if [[ -e $MOUNT_LOCK && $(pgrep -F "$MOUNT_LOCK") ]]; then
    echo "$MOUNT_LOCK is active: ignoring action $ACTION"
    # Do not return a success exit code: it could end up putting the service in 'started' state without doing the mount work (further start commands will be ignored after that)
    exit 1
fi

# See if this drive is already mounted, and if so where
MOUNT_POINT=$(mount | grep -F "${DEVICE}" | awk '{ print $3 }')

# From https://gist.github.com/HazCod/da9ec610c3d50ebff7dd5e7cac76de05
urlencode()
{
    [ -z "$1" ] || echo -n "$@" | hexdump -v -e '/1 "%02x"' | sed 's/\(..\)/%\1/g'
}

do_mount()
{
    if [[ -n ${MOUNT_POINT} ]]; then
        echo "Warning: ${DEVICE} is already mounted at ${MOUNT_POINT}"
        exit 1
    fi

    # Get info for this drive: $ID_FS_LABEL, and $ID_FS_TYPE
    dev_json=$(lsblk -o PATH,LABEL,FSTYPE --json -- "$DEVICE" | jq '.blockdevices[0]')
    ID_FS_LABEL=$(jq -r '.label | select(type == "string")' <<< "$dev_json")
    ID_FS_TYPE=$(jq -r '.fstype | select(type == "string")' <<< "$dev_json")

    # Figure out a mount point to use
    LABEL=${ID_FS_LABEL}
    if [[ -z "${LABEL}" ]]; then
        LABEL=${DEVBASE}
    elif /bin/grep -qF " /run/media/${LABEL} " /etc/mtab; then
        # Already in use, make a unique one
        LABEL+="-${DEVBASE}"
    fi
    MOUNT_POINT="/run/media/${LABEL}"

    echo "Mount point: ${MOUNT_POINT}"

    /bin/mkdir -p -- "${MOUNT_POINT}"

    #### SteamOS Btrfs Begin ####
    if [[ -f /etc/default/steamos-btrfs ]]; then
        source /etc/default/steamos-btrfs
    fi
    if [[ "${ID_FS_TYPE}" == "ext4" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_EXT4_MOUNT_OPTS:-rw,noatime,lazytime}"
    elif [[ "${ID_FS_TYPE}" == "f2fs" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_F2FS_MOUNT_OPTS:-rw,noatime,lazytime,compress_algorithm=zstd,compress_chksum,atgc,gc_merge}"
    elif [[ "${ID_FS_TYPE}" == "btrfs" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_BTRFS_MOUNT_OPTS:-rw,noatime,lazytime,compress-force=zstd,space_cache=v2,autodefrag,ssd_spread,nodiscard}"
    elif [[ "${ID_FS_TYPE}" == "vfat" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_FAT_MOUNT_OPTS:-rw,noatime,lazytime,uid=1000,gid=1000,utf8=1}"
    elif [[ "${ID_FS_TYPE}" == "exfat" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_EXFAT_MOUNT_OPTS:-rw,noatime,lazytime,uid=1000,gid=1000}"
    elif [[ "${ID_FS_TYPE}" == "ntfs" ]]; then
        OPTS="${STEAMOS_BTRFS_SDCARD_NTFS_MOUNT_OPTS:-rw,noatime,lazytime,uid=1000,gid=1000,big_writes,umask=0022,ignore_case,windows_names}"
        EXTRA_OPTS="-t lowntfs-3g"
    else
        echo "Error mounting ${DEVICE}: wrong fstype: ${ID_FS_TYPE} - ${dev_json}"
        exit 2
    fi

    if ! /bin/mount -o "${OPTS}" ${EXTRA_OPTS} -- "${DEVICE}" "${MOUNT_POINT}"; then
        echo "Error mounting ${DEVICE} (status = $?)"
        /bin/rmdir -- "${MOUNT_POINT}"
        exit 1
    fi

    if [[ "${ID_FS_TYPE}" == "btrfs" ]]; then
        # Check if there's a subvolume @ and use that as root of the drive
        if [[ -d "${MOUNT_POINT}/${STEAMOS_BTRFS_SDCARD_BTRFS_MOUNT_SUBVOL:-@}" ]] && btrfs subvolume show "${MOUNT_POINT}/${STEAMOS_BTRFS_SDCARD_BTRFS_MOUNT_SUBVOL:-@}" &>/dev/null; then
            /bin/umount -l -- "${MOUNT_POINT}"
            OPTS+=",subvol=${STEAMOS_BTRFS_SDCARD_BTRFS_MOUNT_SUBVOL:-@}"
            if ! /bin/mount -o "${OPTS}" ${EXTRA_OPTS} -- "${DEVICE}" "${MOUNT_POINT}"; then
                echo "Error mounting ${DEVICE} (status = $?)"
                /bin/rmdir -- "${MOUNT_POINT}"
                exit 1
            fi
        fi
        # Workaround for for Steam compression bug
        for d in "${MOUNT_POINT}"/steamapps/{downloading,temp} ; do
            if ! btrfs subvolume show "$d" &>/dev/null; then
                mkdir -p "$d"
                rm -rf "$d"
                btrfs subvolume create "$d"
                chattr +C "$d"
                chown 1000:1000 "${d%/*}" "$d"
            fi
        done
    elif [[ "${STEAMOS_BTRFS_SDCARD_COMPATDATA_BIND_MOUNT:-0}" == "1" ]] && [[ "${ID_FS_TYPE}" == "vfat" || "${ID_FS_TYPE}" == "exfat" || "${ID_FS_TYPE}" == "ntfs" ]]; then
        # bind mount compatdata folder from internal disk
        mkdir -p "${MOUNT_POINT}"/steamapps/compatdata
        chown 1000:1000 "${MOUNT_POINT}"/steamapps{,/compatdata}
        mkdir -p /home/deck/.local/share/Steam/steamapps/compatdata
        chown 1000:1000 /home/deck/.local{,/share{,/Steam{,/steamapps{,/compatdata}}}}
        /bin/mount --rbind /home/deck/.local/share/Steam/steamapps/compatdata "${MOUNT_POINT}"/steamapps/compatdata
    fi
    #### SteamOS Btrfs End ####

    chown 1000:1000 -- "${MOUNT_POINT}"

    echo "**** Mounted ${DEVICE} at ${MOUNT_POINT} ****"

    url=$(urlencode "${MOUNT_POINT}")

    # If Steam is running, notify it
    if pgrep -x "steam" > /dev/null; then
        # TODO use -ifrunning and check return value - if there was a steam process and it returns -1, the message wasn't sent
        # need to retry until either steam process is gone or -ifrunning returns 0, or timeout i guess
        systemd-run -M 1000@ --user --collect --wait sh -c "./.steam/root/ubuntu12_32/steam steam://addlibraryfolder/${url@Q}"
    fi
}

do_unmount()
{
    url=$(urlencode "${MOUNT_POINT}")

    # If Steam is running, notify it
    if pgrep -x "steam" > /dev/null; then
        # TODO use -ifrunning and check return value - if there was a steam process and it returns -1, the message wasn't sent
        # need to retry until either steam process is gone or -ifrunning returns 0, or timeout i guess
        systemd-run -M 1000@ --user --collect --wait sh -c "./.steam/root/ubuntu12_32/steam steam://removelibraryfolder/${url@Q}"
    fi

    if [[ -z ${MOUNT_POINT} ]]; then
        echo "Warning: ${DEVICE} is not mounted"
    else
        #### SteamOS Btrfs Begin ####
        if mountpoint -q "${MOUNT_POINT}"/steamapps/compatdata; then
            /bin/umount -l -R "${MOUNT_POINT}"/steamapps/compatdata
        fi
        #### SteamOS Btrfs End ####
        /bin/umount -l -- "${DEVICE}"
        echo "**** Unmounted ${DEVICE}"
    fi

    # Delete all empty dirs in /media that aren't being used as mount
    # points. This is kind of overkill, but if the drive was unmounted
    # prior to removal we no longer know its mount point, and we don't
    # want to leave it orphaned...
    for f in /run/media/* ; do
        if [[ -n $(/usr/bin/find "$f" -maxdepth 0 -type d -empty) ]]; then
            if ! /bin/grep -qF " $f " /etc/mtab; then
                echo "**** Removing mount point $f"
                /bin/rmdir "$f"
            fi
        fi
    done
}

case "${ACTION}" in
    add)
        do_mount
        ;;
    remove)
        do_unmount
        ;;
    *)
        usage
        ;;
esac

